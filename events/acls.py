from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"per_page": 1, "query": city + " " + state}
    response = requests.get(url, params=params, headers=headers)
    content = json.load(response.content)
    rediction = {"picture_url": content["photos"][0]["src"]["original"]}
    return rediction


def get_weather_data(city, state):
    pass
